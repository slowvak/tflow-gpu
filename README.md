# README #

This is a Dockerfile that builds a container with:
1. tensorflow with gpu support
3. keras (tensorflow backend)
4. jupyter
5. other nice libs like matplotlib, seaborn, pydotplus, graphviz

To execute in most environments, command would be:
nvidia-docker run --rm  -p 8888:8888 -e KERAS_BACKEND=tensorflow -v /host_notebook_path:/notebook -it slowvak/tensorflow-gpu:latest sh -c "jupyter notebook --ip 0.0.0.0 --allow-root ."

Supported in part by NIH grants CA160045 and DK90728